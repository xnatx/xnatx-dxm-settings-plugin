# XNAT DXM Settings Plugin #

This is the XNAT DXM Settings plugin. Its purpose is to surface project-level settings that control the behavior of external upload applications such as the XNAT Desktop Client. 



# Building & Installing #

To build :

1. If you haven't already, clone this repository and cd to the newly cloned folder.
1. Build the plugin: `./gradlew jar` (on Windows, you can use the batch file: `gradlew.bat jar`). This should build the plugin in the file **build/libs/dxm-settings-plugin-1.0.jar** (the version may differ based on updates to the code).
1. Copy the plugin jar to your plugins folder: `cp build/libs/dxm-settings-plugin-1.0.jar /data/xnat/home/plugins`
1. Restart Tomcat and your plugin will become active in XNAT. 

# Using the Plugin #

Log in to your XNAT and navigate to a project that you intend to upload to. Go to Project Settings, and click on the "Upload Client Settings" tab. You will find a series of settings that you can set. 

See XNAT Documentation: [Setting Project Defaults for Desktop Client Behavior](https://wiki.xnat.org/xnat-tools/xnat-desktop-client-dxm/setting-project-defaults-for-desktop-client-behavior)
