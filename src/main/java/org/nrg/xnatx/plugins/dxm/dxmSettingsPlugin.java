package org.nrg.xnatx.plugins.dxm;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;

@XnatPlugin(value = "dxmSettingsPlugin",
            name = "XNAT DXM Settings Plugin",
            logConfigurationFile = "META-INF/resources/dxm_settings-logback.xml",
            description = "Expose project settings for XNAT Desktop Client")
@ComponentScan("org.nrg.xnatx.plugins.dxm")
@Slf4j
public class dxmSettingsPlugin {
//    @Bean(name = "dxmSettingsThreadPoolExecutorFactoryBean")
//    public ThreadPoolExecutorFactoryBean dxmSettingsThreadPoolExecutorFactoryBean() {
//        final ThreadPoolExecutorFactoryBean tBean = new ThreadPoolExecutorFactoryBean();
//        tBean.setCorePoolSize(5);
//        tBean.setThreadNamePrefix("dxm-settings-");
//        return tBean;
//    }
}
