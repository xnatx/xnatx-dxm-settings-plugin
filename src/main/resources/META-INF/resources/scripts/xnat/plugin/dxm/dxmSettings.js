/*
 * web: dxmSettings.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*!
 * Manage Site-wide Command Configs
 */

console.log('dxmSettings.js');

var XNAT = getObject(XNAT || {});
XNAT.plugin = getObject(XNAT.plugin || {});
XNAT.plugin.dxm = getObject(XNAT.plugin.dxm || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function(){

    function spacer(width){
        return spawn('i.spacer', {
            style: {
                display: 'inline-block',
                width: width + 'px'
            }
        });
    }

    function errorHandler(e, title){
        console.log(e);
        title = (title) ? 'Error Found: '+ title : 'Error';
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': '+ e.statusText+'</strong></p><p>' + e.responseText + '</p>' : e;
        xmodal.alert({
            title: title,
            content: errormsg,
            okAction: function () {
                xmodal.closeAll();
            }
        });
    }

    function getUrlParams(){
        var paramObj = {};

        // get the querystring param, redacting the '?', then convert to an array separating on '&'
        var urlParams = window.location.search.substr(1,window.location.search.length);
        urlParams = urlParams.split('&');

        urlParams.forEach(function(param){
            // iterate over every key=value pair, and add to the param object
            param = param.split('=');
            paramObj[param[0]] = param[1];
        });

        return paramObj;
    }

    function getProjectId(){
        if (XNAT.data.context.projectID.length > 0) return XNAT.data.context.projectID;
        return getUrlParams().id;
    }

    var dxmSetting,
        undefined,
        rootUrl = XNAT.url.rootUrl,
        restUrl = XNAT.url.restUrl,
        csrfUrl = XNAT.url.csrfUrl,
        projectId = getProjectId();

    XNAT.plugin.dxm.dxmSetting = dxmSetting =
        getObject(XNAT.plugin.dxm.dxmSetting || {});

    dxmSetting.settingsList = [
        {
            name: 'require-date',
            label: 'Require Date Verification',
            url: '/data/projects/'+projectId+'/config/applet/require-date',
            default: 'false',
            element: 'switchbox',
            values: 'true|false',
            onText: 'Required',
            offText: 'Not Required (Default)'
        },
        {
            name: 'allow-create-subject',
            label: 'Allow Uploader to Create Subject',
            url: '/data/projects/'+projectId+'/config/applet/allow-create-subject',
            default: 'true',
            element: 'switchbox',
            values: 'true|false',
            onText: 'Enabled (Default)',
            offText: 'Disabled'
        },
        {
            name: 'allow-bulk-uploads',
            label: 'Allow Bulk Uploading',
            url: '/data/projects/'+projectId+'/config/applet/allow-bulk-upload',
            default: 'true',
            element: 'switchbox',
            values: 'true|false',
            onText: 'Enabled (Default)',
            offText: 'Disabled'
        },
        {
            name: 'subject-labeling-scheme',
            label: 'Default Subject Labeling Scheme',
            url: '/data/projects/'+projectId+'/config/applet/default-subject-labeling-scheme',
            default: 'manual',
            element: 'select',
            options: {
                manual: {
                    label: "Manual User Entry (default)",
                    value: "manual"
                },
                auto: {
                    label: "Auto-populate",
                    value: "auto"
                },
                dicomPatientName: {
                    label: "Use DICOM Patient Name (0010,0010)",
                    value: "dicom_patient_name"
                },
                dicomPatientId: {
                    label: "Use DICOM Patient ID (0010,0020)",
                    value: "dicom_patient_id"
                }
            }

        },
        {
            name: 'session-labeling-scheme',
            label: 'Default Session Labeling Scheme',
            url: '/data/projects/'+projectId+'/config/applet/default-session-labeling-scheme',
            default: 'auto',
            element: 'select',
            options: {
                manual: {
                    label: "Manual User Entry",
                    value: "manual"
                },
                auto: {
                    label: "Auto-populate based on Subject Label (default)",
                    value: "auto"
                },
                dicomPatientName: {
                    label: "Use DICOM Accession Num (0008,0050)",
                    value: "dicom_accession_num"
                },
                dicomPatientId: {
                    label: "Use DICOM Patient ID (0010,0020)",
                    value: "dicom_patient_id"
                }
            }

        },
        {
            name: 'labeling-hash',
            // label: 'Hash Function for Labeling (Optional)',
            url: '/data/projects/'+projectId+'/config/applet/labeling-hash',
            default: '',
            element: 'hidden'
        }
    ];

    dxmSetting.postSetting = function(setting,val){
        if (!setting || !val) return false;
        XNAT.xhr.put({
            url: csrfUrl(setting.url+'?inbody=true&accept-not-found=true&status=enabled'),
            data: val.toString(),
            contentType: 'text/plain',
            fail: function(e){
                errorHandler(e,'Cannot update setting for '+setting.name);
            },
            success: function(data){
                console.log('Updated setting for '+setting.name +' to '+val.toString());
            }
        });
    };

    var populateSettings = function(){
        var container = $(document).find('#dxm-settings');
        container.empty();

        function parseResult(data){
            if (data.ResultSet) {
                return data.ResultSet.Result[0].contents.toString() || "false";
            }
            return data.toString();
        }

        function renderSetting(setting){
            var input, classes = 'dxm-setting';
            switch (setting.element){
                case "select":
                    var options = [];
                    Object.keys(setting.options).forEach(function(key){
                        options.push(setting.options[key])
                    });
                    input = XNAT.ui.panel.select.single({
                        name: setting.name,
                        label: setting.label,
                        options: options,
                        description: setting.description || '',
                        addClass: classes
                    });
                    break;
                case "switchbox":
                    var possibleValues = setting.values || 'true|false';
                    possibleValues = possibleValues.split('|');
                    input = XNAT.ui.panel.switchbox({
                        name: setting.name,
                        label: setting.label,
                        addClass: classes,
                        onText: setting.onText || 'true',
                        offText: setting.offText || 'false',
                        value: possibleValues[0]
                    });
                    break;
                case "hidden":
                    input = XNAT.ui.input.hidden({
                        name: setting.name,
                        addClass: classes
                    });
                    break;
                default:
                    input = XNAT.ui.panel.input.text({
                        name: setting.name,
                        label: setting.label,
                        addClass: classes
                    });
            }

            container.append(spawn('!',input.element));
        }
        function populateVal(setting,val){
            // store the preset value to compare when the user clicks "save changes"
            setting['initial-value'] = val;
            switch (setting.element) {
                case "select":
                    $(document).find('select[name='+setting.name+']')
                        .find('option[value='+val+']').prop('selected','selected');
                    break;
                case "switchbox":
                    var $input = $(document).find('input[name='+setting.name+']');
                    if ($input.val() == val) {
                        $input.prop('checked','checked');
                    }
                    else {
                        $input.prop('checked',false);
                    }
                    break;
                default:
                $(document).find('input[name='+setting.name+']')
                    .val(val).data('value',val);
            }
        }

        dxmSetting.settingsList.forEach(function(setting){
            // add placeholder settings
            renderSetting(setting);

            // get existing settings, if set
            XNAT.xhr.getJSON({
                url: rootUrl(setting.url + '?accept-not-found=true'),
                fail: function(e){
                    errorHandler(e,'Cannot get setting for '+setting.name);
                },
                success: function(data,textStatus,jqxhr){
                    // if a setting does not exist when this panel is loaded, initialize the setting by posting the default value
                    if (jqxhr.status === 204 || textStatus === 'no content') {
                        dxmSetting.postSetting(setting,setting.default);
                    }
                    populateVal(setting,parseResult(data || setting.default));
                }
            });
        });
    };

    var populateFooter = function(){
        var footer = $(document).find('#dxm-settings-panel').find('.panel-footer');
        footer.append(
            spawn('!',[
                spawn(
                    'button.primary.pull-right.submit-dxm-settings',
                    {onclick: XNAT.plugin.dxm.dxmSetting.saveChanges },
                    'Save Changes'
                ),
                spawn('div.clearfix.clear')
            ])
        );
    };

    dxmSetting.saveChanges = function(e){
        e.preventDefault();
        var settingsChanged = [], settingVal;
        XNAT.plugin.dxm.dxmSetting.settingsList.forEach(function(setting){
            var preset = setting['initial-value'].toString();
            switch(setting.element){
                case 'select':
                    settingVal = $(document).find('select[name='+setting.name+']').find('option:selected').val();
                    break;
                case 'switchbox':
                    var input = $(document).find('input[name='+setting.name+']');
                    var possibleVals = input.data('values').split('|');
                    var settingVal = (input.prop('checked') === true) ? possibleVals[0] : possibleVals[1];
                    break;
                default:
                    settingVal = $(document).find('input[name='+setting.name+']').val();
            }
            if (settingVal !== preset){
                settingsChanged.push(setting.name);
                XNAT.plugin.dxm.dxmSetting.postSetting(setting,settingVal);
            }
        });

        if (settingsChanged.length) {
            XNAT.ui.banner.top('3000','Updating settings for ' + settingsChanged.join(', '),'success')
        } else {
            XNAT.ui.dialog.message('No changes to save.');
        }
    };

    dxmSetting.init = function(){
        populateFooter();
        populateSettings();
    };

    XNAT.plugin.dxm.dxmSetting.init();

}));